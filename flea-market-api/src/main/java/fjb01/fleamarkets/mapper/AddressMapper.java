package fjb01.fleamarkets.mapper;


import fjb01.fleamarkets.model.dto.addressDTO.DistrictDTO;
import fjb01.fleamarkets.model.dto.addressDTO.ProvinceDTO;
import fjb01.fleamarkets.model.dto.addressDTO.WardDTO;
import fjb01.fleamarkets.model.local.District;
import fjb01.fleamarkets.model.local.Province;
import fjb01.fleamarkets.model.local.Ward;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface AddressMapper {
    ProvinceDTO toDTO(Province province);
    
    DistrictDTO toDTO(District district);

    WardDTO toDTO(Ward ward);

}
