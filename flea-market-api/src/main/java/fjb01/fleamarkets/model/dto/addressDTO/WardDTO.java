package fjb01.fleamarkets.model.dto.addressDTO;

import lombok.Data;

@Data
public class WardDTO {
    private Long id;
    private String name;

    public WardDTO(Long id) {
        this.id = id;
    }
}
