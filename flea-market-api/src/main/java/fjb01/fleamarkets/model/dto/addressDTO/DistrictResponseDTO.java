package fjb01.fleamarkets.model.dto.addressDTO;

import lombok.Data;

@Data
public class DistrictResponseDTO {
    private Long id;
    private String name;

    public DistrictResponseDTO(Long id) {
        this.id = id;
    }
}
