package fjb01.fleamarkets.model.dto.addressDTO;

import lombok.Data;

@Data
public class DistrictDTO {
    private Long id;
    private String name;

    public DistrictDTO(Long id) {
        this.id = id;
    }
}
