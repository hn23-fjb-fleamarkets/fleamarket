package fjb01.fleamarkets.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class TransactionHistory extends BaseEntity{
    private Boolean status;

    @OneToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Transaction transaction;
}
