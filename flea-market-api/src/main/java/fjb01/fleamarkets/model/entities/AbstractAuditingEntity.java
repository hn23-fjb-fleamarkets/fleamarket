package fjb01.fleamarkets.model.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import fjb01.fleamarkets.constant.DateTimeConstant;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@MappedSuperclass
@Data
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditingEntity {

    @CreatedBy
    protected String createdBy;
    @CreatedDate
//    @NotBlank(message = ERROR_REQUIRED)
    @JsonFormat(pattern = DateTimeConstant.DATE_FORMAT_JSON)
    protected LocalDateTime createdDate;
    @LastModifiedBy
    protected String lastModifiedBy;

    @LastModifiedDate
    @JsonFormat(pattern = DateTimeConstant.DATE_FORMAT_JSON)
    protected LocalDateTime lastModifiedDate;
    protected Boolean deleted;

    public boolean isDeleted() {
        return true;
    }
}
