package fjb01.fleamarkets.model.entities;


import fjb01.fleamarkets.model.appEnum.UserRoleEnum;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
public class Account extends BaseEntity {
    @Column(nullable = false)
    @Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,}$")
    private String email;
    @Pattern(regexp = "\\S*", message = "Password must not contain spaces!")
    private String password;
    private Boolean active;
    @Enumerated(EnumType.STRING)
    private UserRoleEnum role;

    private String activeToken;

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;


    @OneToMany(mappedBy = "account")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<Token> tokens;

}
