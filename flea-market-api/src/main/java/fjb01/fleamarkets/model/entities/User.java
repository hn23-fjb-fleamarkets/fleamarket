package fjb01.fleamarkets.model.entities;


import fjb01.fleamarkets.model.local.District;
import fjb01.fleamarkets.model.local.Province;
import fjb01.fleamarkets.model.local.Ward;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Where;

import java.time.LocalDate;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class User extends BaseEntity {
    private String fullName;
    private LocalDate dOB;
    @Pattern(regexp = "^(0[35789][0-9]{8}|02[0-9]{9})$")
    private String phoneNumber;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Province city;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private District district;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Ward ward;

    private String street;

    private String imageProfile;
    @OneToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Account account;

    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<Feedback> feedBacks;

    @OneToMany(mappedBy = "customer")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<Transaction> buyTransactions;

    @OneToMany(mappedBy = "seller")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Where(clause = "deleted = false")
    private List<Transaction> sellTransactions;

}
