package fjb01.fleamarkets.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
@Entity
public class ImageEntity extends BaseEntity {
    private String filePath;
    private Boolean isMainImage;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Category category;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Product product;
}
