package fjb01.fleamarkets.model.appEnum;

public enum UserRoleEnum {
    ADMIN, PARENTS, SCHOOL_OWNER
}
