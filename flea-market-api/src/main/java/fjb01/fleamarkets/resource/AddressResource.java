package fjb01.fleamarkets.resource;


import fjb01.fleamarkets.mapper.AddressMapper;
import fjb01.fleamarkets.model.dto.addressDTO.DistrictDTO;
import fjb01.fleamarkets.model.dto.addressDTO.ProvinceDTO;
import fjb01.fleamarkets.model.dto.addressDTO.WardDTO;
import fjb01.fleamarkets.model.local.District;
import fjb01.fleamarkets.model.local.Province;
import fjb01.fleamarkets.model.local.Ward;
import fjb01.fleamarkets.services.AddressService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/local")
public class AddressResource {
    private final AddressService addressService;
    private final AddressMapper mapper;

    public AddressResource(AddressService addressService, AddressMapper mapper) {
        this.addressService = addressService;
        this.mapper = mapper;
    }

    @GetMapping("/pro")
    public ResponseEntity<?> getProvince() {

        List<Province> provinceList = addressService.getAllProvince();
        if(provinceList.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<ProvinceDTO> provinceDTOS = provinceList.stream().map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(provinceDTOS);
    }

    @GetMapping("/dis/{id}")
    public ResponseEntity<?> getDistrict(@PathVariable Long id) {
        List<District> districts = addressService.getAllDistrictByProvinceId(id);
        if(districts.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<DistrictDTO> districtDTOS = districts.stream().map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(districtDTOS);
    }

    @GetMapping("/war/{id}")
    public ResponseEntity<?> getWard(@PathVariable Long id) {

        List<Ward> wards = addressService.getAllWardByDistrictId(id);
        if(wards.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<WardDTO> wardDTOS = wards.stream().map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(wardDTOS);
    }
}