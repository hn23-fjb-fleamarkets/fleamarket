package fjb01.fleamarkets.component;

public interface PasswordEncrypt {
    String encrypt(String password);
}
