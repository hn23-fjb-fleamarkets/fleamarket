package fjb01.fleamarkets.config;


import fjb01.fleamarkets.exception.EmailSendingException;
import fjb01.fleamarkets.exception.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.FileNotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler({FileNotFoundException.class, ResourceNotFoundException.class})
    public ResponseEntity<?> notFoundHandle() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(EmailSendingException.class)
    public ResponseEntity<?> emailSendingException(){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred while sending email !");
    }
}
