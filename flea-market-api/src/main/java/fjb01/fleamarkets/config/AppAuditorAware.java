package fjb01.fleamarkets.config;

import fjb01.fleamarkets.security.SecurityUtil;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AppAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Optional<String> rs = SecurityUtil.getCurrentAccountOpt();
        if (rs.isPresent()) {
            return rs;
        }
        return Optional.of("system");

    }
}
