package fjb01.fleamarkets.security;

import fjb01.fleamarkets.model.entities.User;
import fjb01.fleamarkets.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findUserByAccountEmailAndDeletedFalse(email);
        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("Username is not exist!");
        }
        User user = userOptional.get();
        return new org.springframework.security.core.userdetails.User(
                user.getAccount().getEmail(),
                user.getAccount().getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getAccount().getRole().name()))
        );
    }
}
