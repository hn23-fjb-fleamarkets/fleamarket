package fjb01.fleamarkets.security;


import fjb01.fleamarkets.exception.UnAuthorizedException;
import fjb01.fleamarkets.model.appEnum.UserRoleEnum;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SecurityUtil {
    public static Optional<String> getCurrentAccountOpt() {
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        Object object = authentication.getPrincipal();
        if (object instanceof User) {
            return Optional.of(((User) object).getUsername());
        }
        return Optional.empty();
    }

    public static String getCurrentAccount() {
        return getCurrentAccountOpt().orElseThrow(UnAuthorizedException::new);
    }

    public static Optional<UserRoleEnum> getRoleCurrentAccountOpt() {
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        // ROLE_ADMIN, ROLE_PARENTS, ROLE_SCHOOL_OWNER
        return authentication.getAuthorities().stream()
                .map(authority -> UserRoleEnum.valueOf(
                        authority.getAuthority().replaceAll("ROLE_", "")))
                .findFirst();
    }

    public static UserRoleEnum getRoleCurrentAccount() {
        return getRoleCurrentAccountOpt().orElseThrow(UnAuthorizedException::new);
    }

    public static boolean isAdmin() {
        return getRoleCurrentAccount() == UserRoleEnum.ADMIN;
    }

    public static boolean isParents() {
        return getRoleCurrentAccount() == UserRoleEnum.PARENTS;
    }

    public static boolean isSchoolOwner() {
        return getRoleCurrentAccount() == UserRoleEnum.SCHOOL_OWNER;
    }


}
