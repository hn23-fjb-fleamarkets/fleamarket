package fjb01.fleamarkets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FleamarketsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FleamarketsApplication.class, args);
    }

}
