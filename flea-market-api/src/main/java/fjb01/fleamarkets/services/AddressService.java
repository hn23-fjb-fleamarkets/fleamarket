package fjb01.fleamarkets.services;


import fjb01.fleamarkets.model.local.District;
import fjb01.fleamarkets.model.local.Province;
import fjb01.fleamarkets.model.local.Ward;
import java.util.List;
import java.util.Optional;

public interface AddressService {
    List<Province> getAllProvince();

    Optional<Province> getProvinceById(Long id);

    Optional<District> getDistrictById(Long id);

    Optional<Ward> getWardById(Long id);


    List<District> getAllDistrictByProvinceId(Long id);

    List<Ward> getAllWardByDistrictId(Long id);
}
