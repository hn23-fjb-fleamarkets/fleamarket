package fjb01.fleamarkets.services;

import fjb01.fleamarkets.model.entities.ImageEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ImageEntityService {
    List<ImageEntity> findAllByIdIn(List<Long> ids);
    Optional<ImageEntity> findById(Long id);
//    List<ImageEntity> handleUpdateImage(AdminSchoolRequestDTO schoolRequest, School school, MultipartFile mainImage,
//                      MultipartFile[] files, String folderImage) throws IOException;
    ResponseEntity<?> validateImage(MultipartFile image);
}
