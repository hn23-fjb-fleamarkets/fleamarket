package fjb01.fleamarkets.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public interface FileStorageService {
    String saveFile(MultipartFile file, String folder) throws IOException;
    Resource loadFileAsResource(String relativePath) throws MalformedURLException, FileNotFoundException;
}
