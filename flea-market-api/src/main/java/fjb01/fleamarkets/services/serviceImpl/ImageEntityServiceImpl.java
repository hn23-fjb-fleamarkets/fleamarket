package fjb01.fleamarkets.services.serviceImpl;


import fjb01.fleamarkets.model.entities.ImageEntity;
import fjb01.fleamarkets.repository.ImageEntityRepository;
import fjb01.fleamarkets.services.FileStorageService;
import fjb01.fleamarkets.services.ImageEntityService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ImageEntityServiceImpl implements ImageEntityService {
    private final List<String> allowedImageTypes = Arrays.asList("image/jpeg", "image/png");
    private final FileStorageService fileStorageService;
    private final ImageEntityRepository imageEntityRepository;

    public ImageEntityServiceImpl(FileStorageService fileStorageService, ImageEntityRepository imageEntityRepository) {
        this.fileStorageService = fileStorageService;
        this.imageEntityRepository = imageEntityRepository;
    }

    @Override
    public List<ImageEntity> findAllByIdIn(List<Long> ids) {
        return imageEntityRepository.findAllByIdInAndDeletedFalse(ids);
    }

    @Override
    public Optional<ImageEntity> findById(Long id) {
        return imageEntityRepository.findByIdAndDeletedFalse(id);
    }

//    @Override
//    public List<ImageEntity> handleUpdateImage(AdminSchoolRequestDTO schoolRequest, School school, MultipartFile mainImage,
//                                               MultipartFile[] files, String folderImage) throws IOException {
//        List<Long> idsRequest = new ArrayList<>(schoolRequest.getOldImagesId() == null ? Collections.emptyList() : schoolRequest.getOldImagesId());
//        List<Long> ids = idsRequest.stream().filter(id -> id > 0).collect(Collectors.toList());
//        Long mainImageId = schoolRequest.getOldMainImageId();
//        if (mainImageId != null) {
//            ids.add(mainImageId);
//        }
//        List<ImageEntity> imageEntities = school.getImages();
//        imageEntities.forEach(img -> img.setDeleted(!ids.contains(img.getId())));
//        if (files != null) {
//            for (MultipartFile file : files) {
//                if (file == null || file.isEmpty()) {
//                    break;
//                }
//                String filePath = fileStorageService.saveFile(file, folderImage);
//                ImageEntity imageEntity = new ImageEntity();
//                imageEntity.setSchool(school);
//                imageEntity.setIsMainImage(false);
//                imageEntity.setFilePath(filePath);
//                imageEntity.setDeleted(false);
//                imageEntities.add(imageEntity);
//            }
//        }
//        if (mainImageId == null && mainImage != null) {
//            String filePath = fileStorageService.saveFile(mainImage, folderImage);
//            ImageEntity imageEntity = new ImageEntity();
//            imageEntity.setSchool(school);
//            imageEntity.setIsMainImage(true);
//            imageEntity.setFilePath(filePath);
//            imageEntity.setDeleted(false);
//            imageEntities.add(imageEntity);
//        }
//        school.setImages(imageEntities);
//        return imageEntities;
//    }

    public ResponseEntity<?> validateImage(MultipartFile image) {
        if (image == null || image.isEmpty()) {
            return null;
        }
        if (!allowedImageTypes.contains(image.getContentType())) {
            return ResponseEntity.badRequest().body("Only image files (JPEG, PNG) are allowed.");
        }
        if (image.getSize() > 5 * 1024 * 1024) {
            return ResponseEntity.badRequest().body("Maximum file size is 5MB.");
        }
        return null;
    }
}
