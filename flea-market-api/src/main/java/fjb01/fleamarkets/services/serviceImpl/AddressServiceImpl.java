package fjb01.fleamarkets.services.serviceImpl;


import fjb01.fleamarkets.model.local.District;
import fjb01.fleamarkets.model.local.Province;
import fjb01.fleamarkets.model.local.Ward;
import fjb01.fleamarkets.repository.DistrictRepository;
import fjb01.fleamarkets.repository.ProvinceRepository;
import fjb01.fleamarkets.repository.WardRepository;
import fjb01.fleamarkets.services.AddressService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {
    private final ProvinceRepository provinceRepository;
    private final DistrictRepository districtRepository;
    private final WardRepository wardRepository;

    public AddressServiceImpl(ProvinceRepository provinceRepository, DistrictRepository districtRepository, WardRepository wardRepository) {
        this.provinceRepository = provinceRepository;
        this.districtRepository = districtRepository;
        this.wardRepository = wardRepository;
    }

    @Override
    public List<Province> getAllProvince() {
        return provinceRepository.getAllByOrderByLevelAsc();
    }

    @Override
    public Optional<Province> getProvinceById(Long id) {
        return provinceRepository.findById(id);
    }

    @Override
    public Optional<District> getDistrictById(Long id) {
        return districtRepository.findById(id);
    }

    @Override
    public Optional<Ward> getWardById(Long id) {
        return wardRepository.findById(id);
    }

    @Override
    public List<District> getAllDistrictByProvinceId(Long id) {
        return districtRepository.getAllByProvinceId(id);
    }

    @Override
    public List<Ward> getAllWardByDistrictId(Long id) {
        return wardRepository.getAllByDistrictId(id);
    }

}
