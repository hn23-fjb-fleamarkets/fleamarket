package fjb01.fleamarkets.services.serviceImpl;

import fjb01.fleamarkets.services.FileStorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@Service
public class FileLocalStorageService implements FileStorageService {
    @Value("${app.file.location}")
    String fileLocation;
    @Override
    public String saveFile(MultipartFile file, String folder) throws IOException {
        String originalFilename = file.getOriginalFilename();
        assert originalFilename != null;
        String newFilename = generateNewFilename(originalFilename); // Tạo tên mới cho tệp

        Path relativePath = getRelativePath(folder, newFilename); // Đường dẫn tới tệp mới
        createFolder(folder); // Tạo thư mục nếu chưa tồn tại
        file.transferTo(Paths.get(fileLocation).resolve(relativePath));

        return relativePath.toString();
    }

    @Override
    public Resource loadFileAsResource(String relativePath) throws MalformedURLException, FileNotFoundException {
        Path absolutePath = Paths.get(fileLocation).resolve(relativePath);
        Resource resource = new UrlResource(absolutePath.toUri());
        if (resource.exists()) {
            return resource;
        }
        throw new FileNotFoundException("Cant not find file with path" + relativePath);
    }
    private String generateNewFilename(String originalFilename) {
        String fileExtension = originalFilename.substring(originalFilename.lastIndexOf("."));
        String newFilename = UUID.randomUUID().toString(); // Sử dụng UUID làm tên mới
        return newFilename + fileExtension;
    }
    private void createFolder(String folder) throws IOException {
        if (StringUtils.hasText(folder)) {
            Path folderPart = Paths.get(fileLocation).resolve(folder);
            if (Files.notExists(folderPart)) {
                Files.createDirectories(folderPart);
            }
        }
    }
    private Path getRelativePath(String folder, String fileName) {
        Objects.requireNonNull(fileName);
        return StringUtils.hasText(folder) ? Paths.get(folder).resolve(fileName) : Paths.get(fileName);
    }
}