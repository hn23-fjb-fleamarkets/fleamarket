package fjb01.fleamarkets.constant;

import java.time.LocalDate;

public final class EmailContextConstant {
    public static final String REGISTER_SUBJECT = "KINDERGARTEN Verify Account "+ LocalDate.now();
    public static final String REGISTER_TEXT = "<p>Please verify your email address to complete the <b>Kindergarten suggest</b>" +
                                                " account registration!</p>";
    public static final String RESET_SUBJECT="KINDERGARTEN Reset Password "+ LocalDate.now();
    public static final String RESET_TEXT="<b>For your security, the link will expire in 24 hours or immediately after you reset your password.</b><br>";

    public static final String REQUEST_SUBJECT="KINDERGARTEN Request Counselling "+ LocalDate.now();
    public static final String REQUEST_TEXT="Thank you for your request. We'll contact to you as soon as possible.";
    public static final String CREATE_USER_SUBJECT="no-reply-email-KTS-system <Account created> "+ LocalDate.now();
    public static final String SCHOOL_SUBJECT="no-reply-email-KTS-system <Interview schedule title> "+ LocalDate.now();
    public static final String SCHOOL_BODY_BEGIN="<html><body><p>This email is from KTS system,</p><br>";
    public static final String TEXT_BEGIN="<html><body>"+
                                        "Dear Mr/Mrs ";
    public static final String TEXT_END= "<br><br>"+
                                        "<p>Thanks & Regards!</p>" +
                                        "<b>KTS Team</b>"+
                                        "</body></html>";
}