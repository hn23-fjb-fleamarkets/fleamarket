package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.local.Ward;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WardRepository extends BaseRepository<Ward,Long> {
    List<Ward> getAllByDistrictId(Long id);

}