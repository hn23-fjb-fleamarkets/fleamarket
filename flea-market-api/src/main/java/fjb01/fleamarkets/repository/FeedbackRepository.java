package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.entities.Feedback;

public interface FeedbackRepository extends BaseRepository<Feedback, Long>{
}
