package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.local.District;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRepository extends BaseRepository<District,Long> {
    List<District> getAllByProvinceId(Long id);
}
