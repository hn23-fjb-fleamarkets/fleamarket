package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.entities.Token;

public interface TokenRepository extends BaseRepository<Token, Long>{
}
