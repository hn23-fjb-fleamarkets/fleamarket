package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.entities.Product;

public interface ProductRepository extends BaseRepository<Product, Long>{
}
