package fjb01.fleamarkets.repository;


import fjb01.fleamarkets.model.appEnum.UserRoleEnum;
import fjb01.fleamarkets.model.entities.Account;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends BaseRepository<Account, Long>{
    boolean existsAccountByEmailAndDeletedFalse(String email);
    Optional<Account> findByEmailAndPasswordAndActiveTrueAndDeletedFalse(String email, String password);
    Optional<Account> findByEmailAndActiveTrueAndDeletedFalse(String email);
    Optional<Account> findByEmailAndActiveFalseAndDeletedFalse(String email);
    List<Account> findAllByRoleAndDeletedFalse(UserRoleEnum role);
}