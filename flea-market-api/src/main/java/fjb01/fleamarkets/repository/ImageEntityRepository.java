package fjb01.fleamarkets.repository;


import fjb01.fleamarkets.model.entities.ImageEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ImageEntityRepository extends BaseRepository<ImageEntity, Long> {
    List<ImageEntity> findAllByIdInAndDeletedFalse(List<Long> ids);
    Optional<ImageEntity> findByIdAndDeletedFalse(Long id);
}
