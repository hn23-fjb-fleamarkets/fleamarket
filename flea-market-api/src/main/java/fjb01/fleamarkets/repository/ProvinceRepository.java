package fjb01.fleamarkets.repository;


import fjb01.fleamarkets.model.local.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ProvinceRepository extends JpaRepository<Province,Long> {
    Optional<Province> getProvinceById(Long Id);
    List<Province> getAllByOrderByLevelAsc();

}