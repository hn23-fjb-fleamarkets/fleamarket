package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.entities.Category;

public interface CategoryRepository extends BaseRepository<Category, Long>{
}
