package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.entities.TransactionHistory;

public interface TransactionHistoryRepository extends BaseRepository<TransactionHistory, Long>{
}
