package fjb01.fleamarkets.repository;


import fjb01.fleamarkets.model.appEnum.UserRoleEnum;
import fjb01.fleamarkets.model.entities.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;



@Repository
public interface UserRepository extends BaseRepository<User, Long> {

    List<User> findAllByAccount_RoleAndDeletedFalse(UserRoleEnum role);

    Optional<User> findUserByAccountEmailAndDeletedFalse(String email);

    Optional<User> findUserByAccount_RoleAndDeletedFalse(UserRoleEnum role);

    Optional<User> findUserAndAccountByAccountEmailAndDeletedFalse(String email);

    Optional<User> findByIdAndDeletedFalse(Long id);

    Optional<User> findByPhoneNumber(String phoneNumber);

    boolean existsByPhoneNumberAndDeletedFalse(String phoneNumber);


}
