package fjb01.fleamarkets.repository;

import fjb01.fleamarkets.model.entities.Transaction;

public interface TransactionRepository extends BaseRepository<Transaction, Long>{
}
